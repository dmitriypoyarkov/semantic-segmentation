import time

import cv2 as cv

from semantic_segmentation.segmentator import SemanticSegmentator

if __name__ == '__main__':
    seg = SemanticSegmentator(
        label_colors=[
            (0, 0, 0),
            (255, 0, 0),
            (0, 0, 0),
            (105, 105, 105),
            (0, 255, 0),
            (0, 255, 0),
            (0, 0, 0),
            (0, 0, 0),
            (0, 0, 0),
            (0, 0, 0),
            (0, 0, 0),
            (0, 0, 0)
        ]
    )
    img = cv.imread('test.png')

    n_tries = 10
    time_measurements = []
    for i in range(n_tries):
        start = time.time()
        segmented = seg.process_image(img)
        time_measurement = time.time() - start
        time_measurements.append(time_measurement)
        print(f'Try {i}: time spent {time_measurement:.02f}')

    total_time = sum(time_measurements)
    print(f'Total time: {total_time:.02f}\nAverage time: {(total_time/n_tries):.02f}')

    # cv.imshow('Press any key to close images', img)
    # cv.imshow('Press any key to close images...', segmented)
    # print('Press any key to close images...')
    # cv.waitKey(0)
    # cv.destroyAllWindows()