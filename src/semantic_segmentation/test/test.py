import cv2 as cv

from semantic_segmentation.segmentator import SemanticSegmentator

if __name__ == '__main__':
    seg = SemanticSegmentator(
        label_colors=[
            (0, 0, 0),
            (255, 0, 0),
            (0, 0, 0),
            (105, 105, 105),
            (0, 255, 0),
            (0, 255, 0),
            (0, 0, 0),
            (0, 0, 0),
            (0, 0, 0),
            (0, 0, 0),
            (0, 0, 0),
            (0, 0, 0)
        ]
    )
    img = cv.imread('test.png')
    segmented = seg.process_image(img)
    cv.imshow('Press any key to close images', img)
    cv.imshow('Press any key to close images...', segmented)
    print('Press any key to close images...')
    cv.waitKey(0)
    cv.destroyAllWindows()