import os

import cv2 as cv
import numpy as np
import rospy
from cv2.typing import MatLike
from cv_bridge import CvBridge
from sensor_msgs.msg import Image

from semantic_segmentation.demos.common.python.openvino.model_zoo.model_api.adapters import (
    OpenvinoAdapter, OVMSAdapter, create_core)
from semantic_segmentation.demos.common.python.openvino.model_zoo.model_api.models import (
    OutputTransform, SegmentationModel)
from semantic_segmentation.demos.common.python.openvino.model_zoo.model_api.pipelines import (
    AsyncPipeline, get_user_config)


def resolve_path(path : str):
    return os.path.normpath(
        os.path.join(
            os.path.dirname(
                os.path.abspath(__file__)
            ),
            path
        )
    )


class SemanticSegmentator(object):
    def __init__(
            self,
            label_colors,
            model='models/intel/icnet-camvid-ava-0001/FP32/icnet-camvid-ava-0001.xml',
            # model="/home/fc/catkin_ws/src/semantic_segmentation/src/semantic_segmentation/models/semantic-segmentation-adas-0001/FP16/semantic-segmentation-adas-0001.xml",
        ):
        model_path = resolve_path(model)
        plugin_config = get_user_config('AUTO', '', None) # type: ignore
        model_adapter = OpenvinoAdapter(create_core(), model_path, device='AUTO', plugin_config=plugin_config,
                                        max_num_requests=1, model_parameters = {'input_layouts': None})
        self._model = SegmentationModel.create_model('segmentation', model_adapter)
        self._pipeline = AsyncPipeline(self._model)
        self._label_colors = label_colors

        self._frame_id = 0


    def load_palette(self, path):
        color_labels = []

        with open(path, 'r') as file:
            for line in file:
                parts = line.split('#')
                rgb_values = tuple(map(int, parts[0].strip().strip('()').split(',')))
                color_labels.append(rgb_values)
        return color_labels


    def apply_palette(self, label_img : MatLike) -> MatLike:
        ''' Convert label_img (matrix with label numbers (like 0, 1, 2, 3)) to matrix with colors. '''
        colorful_img : MatLike = np.zeros(
            (label_img.shape[0], label_img.shape[1], 3),
            dtype=np.uint8
        )
        for label_index in range((len(self._label_colors))):
            colorful_img[label_img == label_index] = np.array(self._label_colors[label_index])
        return colorful_img


    def process_image(self, img : MatLike):
        if self._pipeline.is_ready():
            self._pipeline.submit_data(img, self._frame_id, {'frame': img})

            if self._pipeline.callback_exceptions:
                raise self._pipeline.callback_exceptions[0]
            # Process all completed requests
            while True:
                results = self._pipeline.get_result(self._frame_id)
                if results:
                    break
            if results:
                label_img, frame_meta = results
            else:
                raise RuntimeError('SEGMENTATOR ERROR: NO RESULTS')
            self._frame_id += 1
            return self.apply_palette(label_img)
        raise RuntimeError('SEGMENTATOR ERROR: PIPELINE NOT READY')


# if __name__ == '__main__':
#     bridge = CvBridge()

#     segm = SemanticSegmentator()
#     rospy.init_node('segmentator_test')

#     def img_callback(imgmsg):
#         img = bridge.imgmsg_to_cv2(imgmsg)


#     rospy.Subscriber('/ritrover/sensors/oakd_fl/rgb/image', Image, img_callback)
#     rospy.spin()