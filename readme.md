# Установка:
1. Клонировать в Catkin Workspace репозиторий:
```bash
git clone https://gitlab.com/dmitriypoyarkov/semantic-segmentation/
```

2. Выполнить:
```bash
catkin build
```

3. Установить openvino-dev:
```bash
pip install openvino-dev
```

4. Установить omz_downloader:
```bash
cd semantic-segmentation
cd src/semantic_segmentation/tools/model_tools/
pip install --upgrade pip
pip install .
```
**Установка завершится с сообщением об ошибке**, но нужные инструменты установятся.
Чтобы проверить это, введите команду:
```bash
omz_downloader
```
Должна появиться инструкция по использованию команды.

5. Загрузить модели:
```bash
cd semantic-segmentation
cd src/semantic_segmentation/models/
omz_downloader --list models.lst
```

6. Проверить работу на тестовой картинке:
```bash
cd semantic-segmentation
cd src/semantic_segmentation/test/
python3 test.py
```
Запуск скрипта должен пройти без ошибок, а в результате программа должна показать две картинки - оригинал и сегментированную версию:  
<img src='readme_img/seg_result.png' height='200px'>  